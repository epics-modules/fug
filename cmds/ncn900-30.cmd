require essioc
require fug
require iocmetadata

epicsEnvSet("P",               "Test:")
epicsEnvSet("R",               "Fug:")
epicsEnvSet("DEVICE_IP",       "127.0.0.1")
epicsEnvSet("CN",              "PORT")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

drvAsynIPPortConfigure($(CN), "$(DEVICE_IP):2101")

epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB)")

dbLoadRecords("$(fug_DB)/fug_ncn900-30.db", "proto_file=fug.proto, Ch_name=$(CN), P=$(P), R=$(R)")



iocInit()
