
fug
======
European Spallation Source ERIC Site-specific EPICS module : fug

This module provides device support for Releller Power Supply and High voltage Power Supply. 
The hardware documentation is available on folder docs


Dependencies
============
streamdevice 2.8.10

Using the module
================

There are examples showing how to use the module on folder cmds/

Buildind the module
===================

This module should compile as a regular EPICS module.
* Run `make` from the command line.

This module should also compile as an E3 module:
* Install `conda` (https://docs.conda.io/en/latest/miniconda.html)
* Create and activate a conda environment that minimally includes the following packages: `make perl tclx compilers epics-base require`
* Run `make -f Makefile.E3` to build the target using the E3 build process
